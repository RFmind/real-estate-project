using System.Collections.Generic;
using RealEstate.Core.Domain.Entities;
using RealEstate.Core.Application.Interfaces;

namespace RealEstate.Core.Application.Residencies.Queries
{
	public class AllResidenciesQueryHandler
		: IQueryHandler<AllResidenciesQuery, List<Residence>>
	{
		private IRepository<Residence> _repository { get; set; }

		public AllResidenciesQueryHandler(IRepository<Residence> repository)
		{
			_repository = repository;
		}

		public List<Residence> Handle(AllResidenciesQuery query)
		{
			return _repository.GetAll();
		}
	}
}