using System.Collections.Generic;
using RealEstate.Core.Domain.Entities;
using RealEstate.Core.Application.Interfaces;

namespace RealEstate.Core.Application.Residencies.Queries
{
	public class NClosestResidenciesQueryHandler
		: IQueryHandler<NClosestResidenciesQuery, List<Residence>>
	{
		private IRepository<Residence> _repository { get; set; }

		public NClosestResidenciesQueryHandler(IRepository<Residence> repository)
		{
			_repository = repository;
		}

		public List<Residence> Handle(NClosestResidenciesQuery query)
		{
			var nClosestResidencies = new List<Residence>();

			foreach (var residence in _repository.GetAll())
			{
				if (residence.DistanceTo(
					query.Longitude,
					query.Latitude) <= query.MaxDistance)
				{
					nClosestResidencies.Add(residence);
					if (nClosestResidencies.Count == query.NClosest)
					{
						return nClosestResidencies;
					}
				}
			}

			return nClosestResidencies;
		}
	}
}