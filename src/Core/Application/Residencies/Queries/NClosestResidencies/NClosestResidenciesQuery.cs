using RealEstate.Core.Application.Interfaces;

namespace RealEstate.Core.Application.Residencies.Queries
{
	public class NClosestResidenciesQuery : IMessage
	{
		public double Longitude { get; set; }
		public double Latitude { get; set; }
		public int NClosest { get; set; }
		public double MaxDistance { get; set; }
	}
}