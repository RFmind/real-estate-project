using RealEstate.Core.Application.Interfaces;

namespace RealEstate.Core.Application.Residencies.Queries
{
	public class FindResidenceByIdQuery : IMessage
	{
		public int ResidenceId { get; set; }
	}
}