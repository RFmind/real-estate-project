using RealEstate.Core.Domain.Entities;
using RealEstate.Core.Application.Interfaces;
using RealEstate.Core.Application.Exceptions;

namespace RealEstate.Core.Application.Residencies.Queries
{
	public class FindResidenceByIdQueryHandler
		: IQueryHandler<FindResidenceByIdQuery, Residence>
	{
		private readonly IRepository<Residence> _repository;

		public FindResidenceByIdQueryHandler(
			IRepository<Residence> repository)
		{
			_repository = repository;
		}

		public Residence Handle(FindResidenceByIdQuery query)
		{
			if (query.ResidenceId != null && query.ResidenceId != 0)
			{
				var residence = _repository.GetById(query.ResidenceId);

				if (residence != null)
				{
					return residence;
				}
				else
				{
					throw new NotFoundException(nameof(Residence));
				}
			}
			else
			{
				throw new ValidationException(query);
			}
		}
	}
}