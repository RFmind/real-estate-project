
namespace RealEstate.Core.Application.Residencies.Commands
{
	public class ChangeFieldsCommandValidator
	{
		public bool IsValidCommand { get; set; }

		public ChangeFieldsCommandValidator(ChangeFieldsCommand command)
		{
			IsValidCommand = true;

			if (command == null)
			{
				IsValidCommand = false;
			}

			if (command.ResidenceDescription == null || command.ResidenceDescription == "")
			{
				IsValidCommand = false;
			}
			if (command.ResidenceId == 0 || command.ResidenceId == null)
			{
				IsValidCommand = false;
			}
		}
	}
}