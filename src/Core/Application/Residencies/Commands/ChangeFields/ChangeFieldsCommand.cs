using RealEstate.Core.Application.Interfaces;

namespace RealEstate.Core.Application.Residencies.Commands
{
	public class ChangeFieldsCommand : IMessage
	{
		public int ResidenceId { get; set; }
		public string ResidenceDescription { get; set; }
	}
}