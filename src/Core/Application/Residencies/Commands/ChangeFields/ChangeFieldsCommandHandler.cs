using System;
using RealEstate.Core.Domain.Entities;
using RealEstate.Core.Application.Interfaces;
using RealEstate.Core.Application.Exceptions;
using RealEstate.Core.Application.Residencies.Events;

namespace RealEstate.Core.Application.Residencies.Commands
{
	public class ChangeFieldsCommandHandler
		: ICommandHandler<ChangeFieldsCommand>
	{
		private readonly IRepository<Residence> _repository;
		private readonly IEventStore _eventstore;

		public ChangeFieldsCommandHandler(
			IRepository<Residence> repository,
			IEventStore eventstore)
		{
			_repository = repository;
			_eventstore = eventstore;
		}

		public void Handle(ChangeFieldsCommand command)
		{
			var validator = new ChangeFieldsCommandValidator(command);

			if (validator.IsValidCommand)
			{
				var residence = _repository.GetById(command.ResidenceId);

				if (residence != null)
				{
					if (residence.Description == command.ResidenceDescription)
					{
						return;
					}
					_repository.Remove(residence);
					residence.Description = command.ResidenceDescription;
					_repository.Add(residence);

					var descriptionChanged = new ResidenceDescriptionChangedEvent
					{
						ResidenceId = command.ResidenceId,
						CreatedAt = DateTime.Now,
						Version = 1,
						Payload = residence
					};

					_eventstore.Publish(descriptionChanged);
				}
				else
				{
					throw new NotFoundException(nameof(Residence));
				}
			}
			else
			{
				throw new ValidationException(command);
			}
		}
	}
}