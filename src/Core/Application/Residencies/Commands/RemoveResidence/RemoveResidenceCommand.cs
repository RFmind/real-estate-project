using RealEstate.Core.Application.Interfaces;

namespace RealEstate.Core.Application.Residencies.Commands
{
	public class RemoveResidenceCommand : IMessage
	{
		public int Id { get; set; }
	}
}