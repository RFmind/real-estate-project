using System.Collections;

namespace RealEstate.Core.Application.Residencies.Commands
{
	public class RemoveResidenceCommandValidator
	{
		public bool IsValidCommand { get; private set; }

		public RemoveResidenceCommandValidator(RemoveResidenceCommand command)
		{
			var requiredProperties = new ArrayList
			{
				command.Id
			};

			IsValidCommand = true;

			foreach(var requiredProperty in requiredProperties)
			{
				if (requiredProperty == null || (int)requiredProperty == 0)
				{
					IsValidCommand = false;
				}
			}

			if (command.Id <= 0)
			{
				IsValidCommand = false;
			}
		}
	}
}