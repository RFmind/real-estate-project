using System;
using System.Collections.Generic;
using RealEstate.Core.Domain.Entities;
using RealEstate.Core.Domain.ValueObjects;
using RealEstate.Core.Application.Interfaces;
using RealEstate.Core.Application.Exceptions;
using RealEstate.Core.Application.Residencies.Events;

namespace RealEstate.Core.Application.Residencies.Commands
{
	public class RemoveResidenceCommandHandler
		: ICommandHandler<RemoveResidenceCommand>
	{
		private readonly IRepository<Residence> _repository;
		private readonly IEventStore _eventstore;

		public RemoveResidenceCommandHandler(
			IRepository<Residence> repository,
			IEventStore eventstore)
		{
			_eventstore = eventstore;
			_repository = repository;
		}

		public void Handle(RemoveResidenceCommand command)
		{
			var validator = new RemoveResidenceCommandValidator(command);

			if (validator.IsValidCommand)
			{
				var residence = _repository.GetById(command.Id);

				if (residence != null)
				{
					residence = _repository.Remove(residence);
					var removedEvent = new ResidenceRemovedEvent
					{
						ResidenceId = command.Id,
						CreatedAt = DateTime.Now,
						Version = 1,
						Payload = residence
					};
					_eventstore.Publish(removedEvent);
				}
				else
				{
					throw new NotFoundException(nameof(Residence));
				}
			}
			else
			{
				throw new ValidationException(command);
			}
		}
	}
}