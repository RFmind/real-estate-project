using RealEstate.Core.Application.Interfaces;

namespace RealEstate.Core.Application.Residencies.Commands
{
    public class AddResidenceCommand : IMessage
    {
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string Street { get; set; }
        public int HouseNumber { get; set; }
        public string Placename { get; set; }
        public int Postcode { get; set; }
        public string Description { get; set; }
        public string ResidenceType { get; set; }
    }
}
