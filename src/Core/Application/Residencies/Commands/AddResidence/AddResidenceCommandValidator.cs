using System;
using System.Collections;

namespace RealEstate.Core.Application.Residencies.Commands
{
    public class AddResidenceCommandValidator
    {
        public bool IsValidCommand { get; private set; }
        public AddResidenceCommandValidator(AddResidenceCommand command)
        {
            var requiredProperties = new ArrayList
            {
                command.Longitude,
                command.Latitude,
                command.Street,
                command.HouseNumber,
                command.Placename,
                command.Postcode,
                command.Description,
                command.ResidenceType
            };

            IsValidCommand = true;
            foreach(var requiredProperty in requiredProperties)
            {
                if (requiredProperty == null)
                {
                    IsValidCommand = false;
                }
                if (requiredProperty is int && (int)requiredProperty == 0)
                {
                    IsValidCommand = false;
                }
                if (requiredProperty is double && (double)requiredProperty == 0.0)
                {
                    IsValidCommand = false;
                }
            }
        }
    }
}