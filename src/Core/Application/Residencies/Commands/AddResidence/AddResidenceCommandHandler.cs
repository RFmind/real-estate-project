using System;
using System.Collections.Generic;
using RealEstate.Core.Domain.Entities;
using RealEstate.Core.Domain.ValueObjects;
using RealEstate.Core.Application.Interfaces;
using RealEstate.Core.Application.Exceptions;
using RealEstate.Core.Application.Residencies.Events;

namespace RealEstate.Core.Application.Residencies.Commands
{
    public class AddResidenceCommandHandler
        : ICommandHandler<AddResidenceCommand>
    {
        private readonly IRepository<Residence> _repository;
        private readonly IEventStore _eventstore;

        public AddResidenceCommandHandler(
            IRepository<Residence> repository,
            IEventStore eventstore)
        {
            _eventstore = eventstore;
            _repository = repository;
        }

        public void Handle(AddResidenceCommand command)
        {
            var validator = new AddResidenceCommandValidator(command);

            if (validator.IsValidCommand)
            {
                var coordinates = new List<double> {command.Longitude, command.Latitude};
                var address = new Address {
                    Street = command.Street,
                    HouseNumber = command.HouseNumber,
                    Postcode = command.Postcode,
                    Placename = command.Placename
                };
                var residenceType = new ResidenceTypeFactory(command.ResidenceType).Get();
                var newResidence = new Residence {
                    CreatedAt = DateTime.Now,
                    Version = 1,
                    Coordinates = coordinates,
                    Address = address,
                    Description = command.Description,
                    ResidenceType = residenceType
                };
                _repository.Add(newResidence);
                _eventstore.Publish(
                    new ResidenceAddedEvent
                    {
                        ResidenceId = newResidence.Id,
                        CreatedAt = DateTime.Now,
                        Version = 1,
                        Payload = newResidence
                    }
                );
            }
            else
            {
                throw new ValidationException(command);
            }
        }
    }
}
