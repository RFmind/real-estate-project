using System;
using RealEstate.Core.Domain.Entities;
using RealEstate.Core.Application.Interfaces;

namespace RealEstate.Core.Application.Residencies.Events
{
	public class ResidenceRemovedEvent : IMessage
	{
		public int ResidenceId { get; set; }
		public DateTime CreatedAt { get; set; }
		public int Version { get; set; }
		public Residence Payload { get; set; }
	}
}