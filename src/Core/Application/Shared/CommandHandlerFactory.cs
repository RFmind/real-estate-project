using System;
using System.Collections.Generic;
using RealEstate.Core.Application.Interfaces;
using RealEstate.Core.Application.Residencies.Commands;

namespace RealEstate.Core.Application.Shared
{
	public class CommandHandlerFactory : ICommandHandlerFactory
	{
		private Container _container { get; set; }
		private Dictionary<Type,Type> _handlers { get; set; }

		public CommandHandlerFactory(Container container)
		{
			_container = container;
			_handlers = new Dictionary<Type, Type>();
			_handlers.Add(
				typeof(AddResidenceCommand),
				typeof(AddResidenceCommandHandler));
			_handlers.Add(
				typeof(RemoveResidenceCommand),
				typeof(RemoveResidenceCommandHandler));
			_handlers.Add(
				typeof(ChangeFieldsCommand),
				typeof(ChangeFieldsCommandHandler));
		}

		public ICommandHandler<T> GetHandler<T>(T command)
			where T : IMessage
		{
			if (_handlers.ContainsKey(typeof(T)))
			{
				return (ICommandHandler<T>)Activator.CreateInstance(
					_handlers[typeof(T)],
					new object[] {_container.Residencies, _container.EventStore });
			}
			else
			{
				return null;
			}
		}
	}
}