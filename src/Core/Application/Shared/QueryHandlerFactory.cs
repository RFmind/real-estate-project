using System;
using System.Collections.Generic;
using RealEstate.Core.Domain.Entities;
using RealEstate.Core.Application.Interfaces;
using RealEstate.Core.Application.Residencies.Queries;

namespace RealEstate.Core.Application.Shared
{
	public class QueryHandlerFactory : IQueryHandlerFactory
	{
		private readonly IRepository<Residence> _repository;
		private Dictionary<Type,Type> _handlers { get; set; }

		public QueryHandlerFactory(IRepository<Residence> repository)
		{
			_repository = repository;
			_handlers = new Dictionary<Type,Type>();

			_handlers.Add(
				typeof(AllResidenciesQuery),
				typeof(AllResidenciesQueryHandler));
			_handlers.Add(
				typeof(NClosestResidenciesQuery),
				typeof(NClosestResidenciesQueryHandler));
			_handlers.Add(
				typeof(FindResidenceByIdQuery),
				typeof(FindResidenceByIdQueryHandler));
		}

		public IQueryHandler<T,U> GetHandler<T,U>(T command)
			where T: IMessage
		{
			if (_handlers.ContainsKey(typeof(T)))
			{
				return (IQueryHandler<T,U>)Activator.CreateInstance(
					_handlers[typeof(T)],
					new object[] { _repository });
			}
			else
			{
				return null;
			}
		}
	}
}