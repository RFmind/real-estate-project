using RealEstate.Core.Domain.Entities;
using RealEstate.Core.Application.Interfaces;

namespace RealEstate.Core.Application.Shared
{
	public class Container
	{
		public IRepository<Residence> Residencies { get; set; }
		public IEventStore EventStore { get; set; }
	}
}