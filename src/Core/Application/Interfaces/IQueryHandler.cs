
namespace RealEstate.Core.Application.Interfaces
{
    public interface IQueryHandler<TQuery,TModel>
    {
        TModel Handle(TQuery message);
    }
}