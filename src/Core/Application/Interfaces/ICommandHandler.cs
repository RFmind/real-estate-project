
namespace RealEstate.Core.Application.Interfaces
{
    public interface ICommandHandler<T>
    {
        void Handle(T message);
    }
}