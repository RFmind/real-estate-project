
namespace RealEstate.Core.Application.Interfaces
{
    public interface ICommandHandlerFactory
    {
        ICommandHandler<T> GetHandler<T>(T command)
        	where T : IMessage;
    }
}