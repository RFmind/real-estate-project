using System.Collections.Generic;

namespace RealEstate.Core.Application.Interfaces
{
    public interface IRepository<T>
    {
        List<T> GetAll();
        T GetById(int id);
        void Add(T entity);
        T Remove(T entity);
    }
}