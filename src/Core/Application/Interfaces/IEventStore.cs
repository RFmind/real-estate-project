
namespace RealEstate.Core.Application.Interfaces
{
    public interface IEventStore
    {
        void Publish(IMessage message);
    }
}