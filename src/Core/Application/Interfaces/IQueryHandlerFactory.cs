
namespace RealEstate.Core.Application.Interfaces
{
	public interface IQueryHandlerFactory
	{
		IQueryHandler<T,U> GetHandler<T,U>(T query)
			where T : IMessage;
	}
}