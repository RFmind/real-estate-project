using System;

namespace RealEstate.Core.Application.Exceptions
{
    [Serializable]
    public class NotFoundException : Exception
    {
        public NotFoundException() { }
        public NotFoundException(string entityName)
            : base(string.Format("{0} Entity Not Found!", entityName)) { }
    }
}