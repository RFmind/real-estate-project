using System;
using RealEstate.Core.Application.Interfaces;

namespace RealEstate.Core.Application.Exceptions
{
    [Serializable]
    public class ValidationException : Exception
    {
        public ValidationException() {}
        public ValidationException(IMessage command)
            : base(string.Format("Command validation failed. Command = {0}", command.ToString()))
        {}
    }
}