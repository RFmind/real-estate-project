using System;

namespace RealEstate.Core.Domain.Entities
{
    public class Entity
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public int Version { get; set; }
    }
}