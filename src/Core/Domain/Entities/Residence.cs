using System;
using System.Collections.Generic;
using RealEstate.Core.Domain.ValueObjects;

namespace RealEstate.Core.Domain.Entities
{
    public class Residence : Entity
    {
        public List<double> Coordinates { get; set; }
        public int ResidenceNumber { get; set; }
        public Address Address { get; set; }
        public string Description { get; set; }
        public ResidenceType ResidenceType { get; set; }

        public double DistanceTo(double latitude, double longitude)
        {
        	var longitudeDistance = Coordinates[0] - longitude;
        	var latitudeDistance = Coordinates[1] - latitude;

        	var distance = Math.Sqrt(
        		Math.Pow(longitudeDistance, 2) + Math.Pow(latitudeDistance, 2));

        	return Math.Round(distance, 3);
        }
    }
}
