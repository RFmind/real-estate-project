
namespace RealEstate.Core.Domain.ValueObjects
{
    public class ResidenceTypeFactory
    {
        private string _typeString { get; set; }
        public ResidenceTypeFactory(string typeString)
        {
            _typeString = typeString;
        }

        public ResidenceType Get()
        {
            if (_typeString == "Appartment")
            {
                return new AppartmentType();
            }
            // if type not found, return default
            return new AppartmentType();
        }
    }
}