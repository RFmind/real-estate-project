
namespace RealEstate.Core.Domain.ValueObjects
{
    public class Address
    {
        public string Street { get; set; }
        public int HouseNumber { get; set; }
        public int Postcode { get; set; }
        public string Placename { get; set; }
    }
}