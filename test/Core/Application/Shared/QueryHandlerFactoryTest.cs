using Xunit;
using System.Collections.Generic;
using RealEstate.Core.Domain.Entities;
using RealEstate.Core.Application.Shared;
using RealEstate.Core.Application.Interfaces;
using RealEstate.Core.Application.Residencies.Queries;
using RealEstate.Test.Core.Application.Residencies.Commands;

namespace RealEstate.Test.Core.Applcation.Shared
{
	public class TestQuery : IMessage
	{}

	public class QueryHandlerFactoryTest
	{
		[Fact]
		public void ShouldHaveAllResidenciesQueryMapping()
		{
			var repository = new InMemoryResidenceRepository();
			var factory = new QueryHandlerFactory(repository);
			var query = new AllResidenciesQuery {};

			var handler = factory.GetHandler<AllResidenciesQuery, List<Residence>>(query);

			Assert.IsType<AllResidenciesQueryHandler>(handler);
		}

		[Fact]
		public void ShouldHaveNClosestResidenciesQueryMapping()
		{
			var repository = new InMemoryResidenceRepository();
			var factory = new QueryHandlerFactory(repository);
			var query = new NClosestResidenciesQuery {};

			var handler = factory.GetHandler<NClosestResidenciesQuery, List<Residence>>(query);

			Assert.IsType<NClosestResidenciesQueryHandler>(handler);
		}

		[Fact]
		public void ShouldHaveFindResidenceByIdQueryMapping()
		{
			var repository = new InMemoryResidenceRepository();
			var factory = new QueryHandlerFactory(repository);
			var query = new FindResidenceByIdQuery {};

			var handler = factory.GetHandler<FindResidenceByIdQuery, Residence>(query);

			Assert.IsType<FindResidenceByIdQueryHandler>(handler);
		}

		[Fact]
		public void ShouldReturnNullWhenNoMappingFound()
		{
			var repository = new InMemoryResidenceRepository();
			var factory = new QueryHandlerFactory(repository);
			var query = new TestQuery {};

			var handler = factory.GetHandler<IMessage, object>(query);

			Assert.Null(handler);
		}
	}
}