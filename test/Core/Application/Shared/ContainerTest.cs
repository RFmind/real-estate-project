using Xunit;
using RealEstate.Core.Application.Shared;
using RealEstate.Core.Application.Interfaces;
using RealEstate.Test.Core.Application.Residencies.Commands;

namespace RealEstate.Test.Core.Application.Shared
{
	public class ContainerTest
	{
		[Fact]
		void ContainerShouldHaveResidenciesRepository()
		{
			var container = new Container
			{
				Residencies = new InMemoryResidenceRepository()
			};
			Assert.NotNull(container.Residencies);
		}

		[Fact]
		void ContainerShouldHaveEventStore()
		{
			var container = new Container
			{
				EventStore = new TestEventStore()
			};
			Assert.NotNull(container.EventStore);
		}
	}
}