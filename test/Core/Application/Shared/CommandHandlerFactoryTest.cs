using Xunit;
using RealEstate.Core.Application.Shared;
using RealEstate.Core.Application.Interfaces;
using RealEstate.Core.Application.Residencies.Commands;
using RealEstate.Test.Core.Application.Residencies.Commands;

namespace RealEstate.Test.Core.Application.Shared
{
	public class TestCommand : IMessage
	{}

	public class CommandHandlerFactoryTest
	{
		private Container _testContainer()
		{
			var repository = new InMemoryResidenceRepository();
			var eventstore = new TestEventStore();

			return new Container
			{
				Residencies = repository,
				EventStore = eventstore
			};
		}

		[Fact]
		void FactoryShouldHaveAddResidenceCommandMapping()
		{
			var container = _testContainer();
			var factory = new CommandHandlerFactory(container);
			var command = new AddResidenceCommand {};

			var handler = factory.GetHandler(command);
			
			Assert.IsType<AddResidenceCommandHandler>(handler);
		}

		[Fact]
		public void ShouldHaveRemoveResidenceCommandMapping()
		{
			var container = _testContainer();
			var factory = new CommandHandlerFactory(container);
			var command = new RemoveResidenceCommand {};

			var handler = factory.GetHandler(command);

			Assert.IsType<RemoveResidenceCommandHandler>(handler);
		}

		[Fact]
		public void ShouldHaveChangeFieldsCommandMapping()
		{
			var container = _testContainer();
			var factory = new CommandHandlerFactory(container);
			var command = new ChangeFieldsCommand {};

			var handler = factory.GetHandler(command);

			Assert.IsType<ChangeFieldsCommandHandler>(handler);
		}
	
		[Fact]
		void FactoryShouldReturnNullWhenNoMappingFound()
		{
			var container = new Container
			{
				Residencies = new InMemoryResidenceRepository(),
				EventStore = new TestEventStore()
			};
			var factory = new CommandHandlerFactory(container);
			var command = new TestCommand();

			var handler = factory.GetHandler(command);

			Assert.Null(handler);
		}
	}
}