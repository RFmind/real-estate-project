using Xunit;
using RealEstate.Core.Application.Residencies.Queries;

namespace RealEstate.Test.Core.Application.Residencies.Queries
{
	public class NClosestResidenciesQueryTest
	{
		[Fact]
		public void QueryShouldHaveALongitude()
		{
			var query = new NClosestResidenciesQuery
			{
				Longitude = 12.345
			};

			Assert.Equal(12.345, query.Longitude);
		}

		[Fact]
		public void QueryShouldHaveALatitude()
		{
			var query = new NClosestResidenciesQuery
			{
				Latitude = 12.345
			};

			Assert.Equal(12.345, query.Latitude);
		}

		[Fact]
		public void QueryShouldHaveANumberOfResidenciesToGet()
		{
			var query = new NClosestResidenciesQuery
			{
				NClosest = 2
			};

			Assert.Equal(2, query.NClosest);
		}

		[Fact]
		public void QueryShouldHaveAMaxDistance()
		{
			var query = new NClosestResidenciesQuery
			{
				MaxDistance = 23.222
			};

			Assert.Equal(23.222, query.MaxDistance);
		}
	}
}