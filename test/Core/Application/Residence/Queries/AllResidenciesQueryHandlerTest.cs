using Xunit;
using System.Collections.Generic;
using RealEstate.Core.Domain.Entities;
using RealEstate.Core.Domain.ValueObjects;
using RealEstate.Test.Core.Application.Residencies.Commands;
using RealEstate.Core.Application.Residencies.Queries;

namespace RealEstate.Test.Core.Application.Residencies.Queries
{
	public class AllResidenciesQueryHandlerTest
	{
		private Residence _testResidence()
		{
			var coordinates = new List<double> {
				22.333,
				12.456
			};
			var address = new Address {
				Street = "Some street",
				HouseNumber = 12,
				Postcode = 2344,
				Placename = "Somecity"
			};
			var residenceType = new ResidenceTypeFactory("Appartment").Get();

			return new Residence {
				Coordinates = coordinates,
				Address = address,
				Description = "Some description",
				ResidenceType = residenceType
			};
		}

		[Fact]
		public void ShouldReturnAListOfAllResidencies()
		{
			var repository = new InMemoryResidenceRepository();

			var handler = new AllResidenciesQueryHandler(repository);
			var query = new AllResidenciesQuery {};

			Assert.Empty(repository.GetAll());
			Assert.Empty(handler.Handle(query));

			repository.Add(_testResidence());

			Assert.Single(repository.GetAll());
			Assert.Single(handler.Handle(query));
		}
	}
}