using Xunit;
using System.Collections.Generic;
using RealEstate.Core.Domain.Entities;
using RealEstate.Core.Domain.ValueObjects;
using RealEstate.Core.Application.Residencies.Queries;
using RealEstate.Test.Core.Application.Residencies.Commands;

namespace RealEstate.Test.Core.Application.Residencies.Queries
{
	public class NClosestResidenciesQueryHandlerTest
	{
		private Residence _testResidence()
		{
			var coordinates = new List<double>
			{
				22.222,
				33.333
			};
			var address = new Address
			{
				Street = "Some Street",
				HouseNumber = 12,
				Postcode = 2344,
				Placename = "Somecity"
			};
			var residenceType = new ResidenceTypeFactory("Appartment").Get();

			return new Residence
			{
				Coordinates = coordinates,
				Address = address,
				Description = "Some description",
				ResidenceType = residenceType
			};
		}

		[Fact]
		public void ShouldNotReturnMoreThanNResidencies()
		{
			var repository = new InMemoryResidenceRepository();
			for (var i=0; i<3; i++)
			{
				repository.Add(_testResidence());
			}

			var handler = new NClosestResidenciesQueryHandler(repository);
			var query = new NClosestResidenciesQuery
			{
				Longitude = 23.222,
				Latitude = 34.333,
				NClosest = 2,
				MaxDistance = 100.0
			};

			Assert.Equal(3, repository.GetAll().Count);
			Assert.Equal(2, handler.Handle(query).Count);
			Assert.Equal(3, repository.GetAll().Count);
		}

		[Fact]
		public void ShouldReturnEmptyListWhenNoneFound()
		{
			var repository = new InMemoryResidenceRepository();
			var handler = new NClosestResidenciesQueryHandler(repository);
			var query = new NClosestResidenciesQuery
			{
				Longitude = 23.222,
				Latitude = 34.333,
				NClosest = 2,
				MaxDistance = 10.0
			};

			Assert.IsType<List<Residence>>(handler.Handle(query));
			Assert.Empty(handler.Handle(query));
		}
	}
}