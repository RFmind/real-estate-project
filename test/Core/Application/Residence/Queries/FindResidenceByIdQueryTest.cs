using Xunit;
using RealEstate.Core.Application.Residencies.Queries;

namespace RealEstate.Test.Core.Application.Residencies.Queries
{
	public class FindResidenceByIdQueryTest
	{
		[Fact]
		public void QueryShouldHaveAnId()
		{
			var query = new FindResidenceByIdQuery
			{
				ResidenceId = 1
			};
			Assert.Equal(1, query.ResidenceId);
		}
	}
}