using Xunit;
using System;
using System.Collections.Generic;
using RealEstate.Core.Domain.Entities;
using RealEstate.Core.Domain.ValueObjects;
using RealEstate.Core.Application.Exceptions;
using RealEstate.Core.Application.Residencies.Queries;
using RealEstate.Test.Core.Application.Residencies.Commands;

namespace RealEstate.Test.Core.Application.Residencies.Queries
{
	public class FindResidenceByIdQueryHandlerTest
	{
		private Residence _testResidence()
		{
			var coordinates = new List<double>
			{
				22.222,
				33.333
			};
			var address = new Address
			{
				Street = "Some street",
				HouseNumber = 12,
				Postcode = 2344,
				Placename = "Somecity"
			};
			var residenceType = new ResidenceTypeFactory("Appartment").Get();

			return new Residence
			{
				Id = 1,
				CreatedAt = DateTime.Now,
				Version = 1,
				Coordinates = coordinates,
				Address = address,
				Description = "Some description",
				ResidenceType = residenceType
			};
		}

		[Fact]
		public void ShouldReturnAResidenceWhenFound()
		{
			var repository = new InMemoryResidenceRepository();
			var handler = new FindResidenceByIdQueryHandler(repository);
			var query = new FindResidenceByIdQuery
			{
				ResidenceId = 1
			};
			var testResidence = _testResidence();

			repository.Add(testResidence);

			var result = handler.Handle(query);

			Assert.IsType<Residence>(result);
			Assert.Equal(testResidence.Id, result.Id);
			Assert.Equal(testResidence.Description, result.Description);
		}

		[Fact]
		public void ShouldThrowValidationExceptionWhenNoIdGiven()
		{
			var repository = new InMemoryResidenceRepository();
			var handler = new FindResidenceByIdQueryHandler(repository);
			var query = new FindResidenceByIdQuery {};

			Assert.Throws<ValidationException>(
				() => handler.Handle(query));
		}

		[Fact]
		public void ShouldThrowNotFoundException()
		{
			var repository = new InMemoryResidenceRepository();
			var handler = new FindResidenceByIdQueryHandler(repository);
			var query = new FindResidenceByIdQuery
			{
				ResidenceId = 20
			};

			Assert.Throws<NotFoundException>(
				() => handler.Handle(query));
		}
	}
}