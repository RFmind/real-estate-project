using Xunit;
using System;
using RealEstate.Core.Domain.Entities;
using RealEstate.Core.Application.Interfaces;
using RealEstate.Core.Application.Residencies.Events;

namespace RealEstate.Test.Core.Application.Residencies.Events
{
    public class ResidenceRemovedEventTest
    {
        [Fact]
        public void EventShouldHaveAResidenceId()
        {
            var testevent = new ResidenceRemovedEvent
            {
                ResidenceId = 1
            };

            Assert.Equal(1, testevent.ResidenceId);
        }

        [Fact]
        public void EventShouldHaveACreatedAt()
        {
            var createdAt = DateTime.Now;
            var testevent = new ResidenceRemovedEvent
            {
                CreatedAt = createdAt
            };

            Assert.Equal(createdAt, testevent.CreatedAt);
        }

        [Fact]
        public void EventShouldHaveAVersion()
        {
            var version = 1;
            var testevent = new ResidenceRemovedEvent
            {
                Version = version
            };

            Assert.Equal(version, testevent.Version);
        }
    }
}

