using Xunit;
using System;
using RealEstate.Core.Domain.Entities;
using RealEstate.Core.Application.Residencies.Events;

namespace RealEstate.Test.Core.Application.Residencies.Events
{
    public class ResidenceDescriptionChangedEventTest
    {
        [Fact]
        public void EventShouldHaveAResidenceId()
        {
            var testevent = new ResidenceDescriptionChangedEvent
            {
                ResidenceId = 1
            };

            Assert.Equal(1, testevent.ResidenceId);
        }

        [Fact]
        public void EventShouldHaveACreatedAt()
        {
            var createdAt = DateTime.Now;
            var testevent = new ResidenceDescriptionChangedEvent
            {
                CreatedAt = createdAt
            };
            
            Assert.Equal(createdAt, testevent.CreatedAt);
        }

        [Fact]
        public void EventShouldHaveAVersion()
        {
            var version = 1;
            var testevent = new ResidenceDescriptionChangedEvent
            {
                Version = version
            };

            Assert.Equal(version, testevent.Version);
        }

        [Fact]
        public void EventShouldHaveAPayload()
        {
            var payload = new Residence {};
            var testevent = new ResidenceDescriptionChangedEvent
            {
                Payload = payload
            };

            Assert.Equal(payload, testevent.Payload);
        }
    }
}
