using Xunit;
using RealEstate.Core.Application.Residencies.Commands;
using RealEstate.Core.Application.Exceptions;

namespace RealEstate.Test.Core.Application.Residencies.Commands
{
	public class AddResidenceCommandHandlerTest
	{
		[Fact]
		public void HandlerShouldAddWhenCommandIsValid()
		{
			var repository = new InMemoryResidenceRepository();
			var eventstore = new TestEventStore();
			var handler = new AddResidenceCommandHandler(
				repository,
				eventstore);

			var validCmd = new AddResidenceCommand
			{
				Longitude = 23.444,
				Latitude = 22.342,
				Street = "Some street",
				HouseNumber = 45,
				Placename = "Somecity",
				Postcode = 2323,
				Description = "Some description",
				ResidenceType = "Appartment"
			};

			Assert.Empty(eventstore.events);
			Assert.Empty(repository.GetAll());

			handler.Handle(validCmd);

			Assert.Single(eventstore.events);
			Assert.Single(repository.GetAll());
		}

		[Fact]
		public void HandlerShouldThrowValidationExceptionOnInvalidCommand()
		{
			var repository = new InMemoryResidenceRepository();
			var eventstore = new TestEventStore();
			var handler = new AddResidenceCommandHandler(
				repository,
				eventstore);

			var invalidCmd = new AddResidenceCommand{};

			Assert.Empty(eventstore.events);
			Assert.Empty(repository.GetAll());

			Assert.Throws<ValidationException>(
				() => handler.Handle(invalidCmd));
			
			Assert.Empty(eventstore.events);
			Assert.Empty(repository.GetAll());
		}
	}
}
