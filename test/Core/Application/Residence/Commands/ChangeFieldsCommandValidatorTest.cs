using Xunit;
using RealEstate.Core.Application.Residencies.Commands;

namespace RealEstate.Test.Core.Application.Residencies.Commands
{
	public class ChangeFieldsCommandValidatorTest
	{
		[Fact]
		public void ResidenceIdShouldBeRequired()
		{
			var command = new ChangeFieldsCommand
			{
				ResidenceId = 1,
				ResidenceDescription = "Some description"
			};
			var badCommand = new ChangeFieldsCommand
			{
				ResidenceDescription = "Some description"
			};
			var validator = new ChangeFieldsCommandValidator(command);
			var badValidator = new ChangeFieldsCommandValidator(badCommand);

			Assert.True(validator.IsValidCommand);
			Assert.False(badValidator.IsValidCommand);
		}

		[Fact]
		public void ResidenceDescriptionShouldBeRequired()
		{
			var command = new ChangeFieldsCommand
			{
				ResidenceId = 1,
				ResidenceDescription = "Some description"
			};
			var badCommand = new ChangeFieldsCommand
			{
				ResidenceId = 1
			};

			var validator = new ChangeFieldsCommandValidator(command);
			var badValidator = new ChangeFieldsCommandValidator(badCommand);

			Assert.True(validator.IsValidCommand);
			Assert.False(badValidator.IsValidCommand);
		}
	}
}