using Xunit;
using System;
using System.Collections.Generic;
using RealEstate.Core.Domain.Entities;
using RealEstate.Core.Domain.ValueObjects;
using RealEstate.Core.Application.Residencies.Commands;
using RealEstate.Core.Application.Residencies.Events;
using RealEstate.Core.Application.Exceptions;

namespace RealEstate.Test.Core.Application.Residencies.Commands
{
	public class RemoveResidenceCommandHandlerTest
	{
		private Residence _testResidence()
		{
			var coordinates = new List<double>
			{
				22.222,
				33.333
			};
			var address = new Address
			{
				Street = "Some Street",
				HouseNumber = 12,
				Postcode = 2344,
				Placename = "Somecity"
			};
			var residenceType = new ResidenceTypeFactory("Appartment").Get();

			return new Residence
			{
				Id = 1,
				CreatedAt = DateTime.Now,
				Version = 1,
				Coordinates = coordinates,
				Address = address,
				Description = "Some description",
				ResidenceType = residenceType
			};
		}

		[Fact]
		public void HandlerShouldThrowValidationExceptionWhenCommandInvalid()
		{
			var repository = new InMemoryResidenceRepository();
			var eventstore = new TestEventStore();
			var handler = new RemoveResidenceCommandHandler(
				repository,
				eventstore);

			var badCommand = new RemoveResidenceCommand
			{
				Id = -1
			};

			Assert.Throws<ValidationException>(
				() => handler.Handle(badCommand));
		}

		[Fact]
		public void HandlerShouldThrowNotFoundException()
		{
			var repository = new InMemoryResidenceRepository();
			var eventstore = new TestEventStore();
			var handler = new RemoveResidenceCommandHandler(
				repository,
				eventstore);

			var command = new RemoveResidenceCommand
			{
				Id = 1
			};

			Assert.Throws<NotFoundException>(
				() => handler.Handle(command));
		}

		[Fact]
		public void HandlerShouldRemoveResidenceFromRepository()
		{
			var repository = new InMemoryResidenceRepository();
			var eventstore = new TestEventStore();
			var handler = new RemoveResidenceCommandHandler(
				repository,
				eventstore);

			var command = new RemoveResidenceCommand
			{
				Id = 1
			};

			repository.Add(_testResidence());
			Assert.Single(repository.GetAll());

			handler.Handle(command);
			Assert.Empty(repository.GetAll());
		}

		[Fact]
		public void HandlerShouldPublishResidenceRemovedEvent()
		{
			var repository = new InMemoryResidenceRepository();
			var eventstore = new TestEventStore();
			var handler = new RemoveResidenceCommandHandler(
				repository,
				eventstore);

			var command = new RemoveResidenceCommand
			{
				Id = 1
			};

			repository.Add(_testResidence());
			Assert.Single(repository.GetAll());
			Assert.Empty(eventstore.events);

			handler.Handle(command);

			Assert.Single(eventstore.events);
			Assert.IsType<ResidenceRemovedEvent>(eventstore.events[0]);
		}
	}
}