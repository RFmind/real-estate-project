using System.Collections.Generic;
using RealEstate.Core.Domain.Entities;
using RealEstate.Core.Application.Interfaces;

namespace RealEstate.Test.Core.Application.Residencies.Commands
{
	public class InMemoryResidenceRepository : IRepository<Residence>
	{
		private Dictionary<int,Residence> data { get; set; }
		private int _nextId { get; set; }

		public InMemoryResidenceRepository()
		{
			data = new Dictionary<int,Residence>();
			_nextId = 1;
		}

		public InMemoryResidenceRepository(List<Residence> residences)
		{
			foreach (var residence in residences)
			{
				data.Add(residence.Id, residence);
			}
		}

		public List<Residence> GetAll()
		{
			return new List<Residence>(data.Values);
		}

		public Residence GetById(int id)
		{
			if (data.ContainsKey(id))
			{
				return data[id];
			}
			return null;
		}

		public void Add(Residence residence)
		{
			if (residence.Id == null || residence.Id == 0)
			{
				residence.Id = _nextId;
				_nextId = _nextId + 1;
			}
			data.Add(residence.Id, residence);
		}

		public Residence Remove(Residence residence)
		{
			if (data.ContainsKey(residence.Id))
			{
				data.Remove(residence.Id);
			}
			return residence;
		}
	}
}