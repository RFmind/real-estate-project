using Xunit;
using System;
using System.Collections.Generic;
using RealEstate.Core.Domain.Entities;
using RealEstate.Core.Domain.ValueObjects;
using RealEstate.Core.Application.Residencies.Commands;
using RealEstate.Core.Application.Residencies.Events;
using RealEstate.Core.Application.Exceptions;

namespace RealEstate.Test.Core.Application.Residencies.Commands
{
	public class ChangeFieldsCommandHandlerTest
	{
		private Residence _testResidence()
		{
			var coordinates = new List<double>
			{
				22.222,
				33.333
			};
			var address = new Address
			{
				Street = "Some street",
				HouseNumber = 12,
				Postcode = 2344,
				Placename = "Somecity"
			};
			var residenceType = new ResidenceTypeFactory("Appartment").Get();

			return new Residence
			{
				Id = 1,
				CreatedAt = DateTime.Now,
				Version = 1,
				Coordinates = coordinates,
				Address = address,
				Description = "Some description",
				ResidenceType = residenceType
			};
		}

		[Fact]
		public void HandlerShouldThrowValidationExceptionOnInvalidCommand()
		{
			var repository = new InMemoryResidenceRepository();
			var eventstore = new TestEventStore();
			var handler = new ChangeFieldsCommandHandler(
				repository,
				eventstore);

			var command = new ChangeFieldsCommand
			{
				ResidenceId = 1
				//ResidenceDescription = "Some description"
			};

			Assert.Throws<ValidationException>(
				() => handler.Handle(command));
		}

		[Fact]
		public void HandlerShouldThrowNotFoundException()
		{
			var repository = new InMemoryResidenceRepository();
			var eventstore = new TestEventStore();
			var handler = new ChangeFieldsCommandHandler(
				repository,
				eventstore);

			var command = new ChangeFieldsCommand
			{
				ResidenceId = 1,
				ResidenceDescription = "Some description"
			};

			Assert.Throws<NotFoundException>(
				() => handler.Handle(command));
		}

		[Fact]
		public void HandlerShouldChangeResidenceInRepository()
		{
			var repository = new InMemoryResidenceRepository();
			var eventstore = new TestEventStore();
			var handler = new ChangeFieldsCommandHandler(
				repository,
				eventstore);

			var command = new ChangeFieldsCommand
			{
				ResidenceId = 1,
				ResidenceDescription = "Some new description"
			};

			repository.Add(_testResidence());
			Assert.Equal(
				"Some description",
				repository.GetById(1).Description);

			handler.Handle(command);

			Assert.Equal(
				"Some new description",
				repository.GetById(1).Description);
		}

		[Fact]
		public void HandlerShouldEmitDescriptionChangedEvent()
		{
			var repository = new InMemoryResidenceRepository();
			var eventstore = new TestEventStore();
			var handler = new ChangeFieldsCommandHandler(
				repository,
				eventstore);

			var command = new ChangeFieldsCommand
			{
				ResidenceId = 1,
				ResidenceDescription = "Some new description"
			};

			repository.Add(_testResidence());
			Assert.NotEqual(
				repository.GetById(1).Description,
				command.ResidenceDescription);
			Assert.Empty(eventstore.events);

			handler.Handle(command);

			Assert.Equal(
				repository.GetById(1).Description,
				command.ResidenceDescription);
			Assert.Single(eventstore.events);
			Assert.IsType<ResidenceDescriptionChangedEvent>(eventstore.events[0]);
		}

		[Fact]
		public void HandlerShouldNotEmitEventsWhenNothingChanged()
		{
			var repository = new InMemoryResidenceRepository();
			var eventstore = new TestEventStore();
			var handler = new ChangeFieldsCommandHandler(
				repository,
				eventstore);
			var testResidence = _testResidence();

			var command = new ChangeFieldsCommand
			{
				ResidenceId = 1,
				ResidenceDescription = testResidence.Description
			};

			repository.Add(testResidence);
			Assert.Single(repository.GetAll());
			Assert.Empty(eventstore.events);

			handler.Handle(command);

			Assert.Empty(eventstore.events);
		}
	}
}