using Xunit;
using RealEstate.Core.Application.Residencies.Commands;

namespace RealEstate.Test.Core.Application.Residencies.Commands
{
	public class ChangeFieldsCommandTest
	{
		[Fact]
		public void CommandShouldHaveAResidenceId()
		{
			var command = new ChangeFieldsCommand
			{
				ResidenceId = 1
			};

			Assert.Equal(1, command.ResidenceId);
		}

		[Fact]
		public void CommandShouldHavaAResidenceDescription()
		{
			var command = new ChangeFieldsCommand
			{
				ResidenceDescription = "Some description"
			};

			Assert.Equal("Some description", command.ResidenceDescription);
		}
	}
}