using System.Collections.Generic;
using RealEstate.Core.Application.Interfaces;

namespace RealEstate.Test.Core.Application.Residencies.Commands
{
	public class TestEventStore : IEventStore
	{
		public List<IMessage> events { get; set; }

		public TestEventStore()
		{
			events = new List<IMessage>();
		}

		public void Publish(IMessage someevent)
		{
			events.Add(someevent);
		}

		public IMessage GetLastEvent()
		{
			return events[events.Count - 1];
		}
	}
}