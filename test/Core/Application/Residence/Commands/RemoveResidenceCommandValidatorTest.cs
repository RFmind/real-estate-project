using Xunit;
using RealEstate.Core.Application.Residencies.Commands;

namespace RealEstate.Test.Core.Application.Residencies.Commands
{
	public class RemoveResidenceCommandValidatorTest
	{
		[Fact]
		void IdShouldBeRequired()
		{
			var command = new RemoveResidenceCommand
			{
				Id = 1
			};
			var badcommand = new RemoveResidenceCommand {};

			var validator = new RemoveResidenceCommandValidator(command);
			var badValidator = new RemoveResidenceCommandValidator(badcommand);

			Assert.True(validator.IsValidCommand);
			Assert.False(badValidator.IsValidCommand);
		}

		[Fact]
		void IdShouldBeLargerThanN()
		{
			// Should be larget than 0
			var command = new RemoveResidenceCommand
			{
				Id = 1
			};
			var badcommand = new RemoveResidenceCommand {
				Id = 0
			};
			var validator = new RemoveResidenceCommandValidator(command);
			var badvalidator = new RemoveResidenceCommandValidator(badcommand);

			Assert.True(validator.IsValidCommand);
			Assert.False(badvalidator.IsValidCommand);
		}
	}
}