using Xunit;
using RealEstate.Core.Application.Residencies.Commands;

namespace RealEstate.Test.Core.Application.Residencies.Commands
{
	public class AddResidenceCommandTest
	{
		[Fact]
		public void CommandShouldHaveALongitude()
		{
			var command = new AddResidenceCommand
			{
				Longitude = 22.33
			};

			Assert.Equal(22.33, command.Longitude);
		}

		[Fact]
		public void CommandShouldHaveALatitude()
		{
			var command = new AddResidenceCommand
			{
				Latitude = 22.33
			};

			Assert.Equal(22.33, command.Latitude);
		}

		[Fact]
		public void CommandShouldHaveAStreet()
		{
			var command = new AddResidenceCommand
			{
				Street = "Some Street"
			};

			Assert.Equal("Some Street", command.Street);
		}

		[Fact]
		public void CommandShouldHaveAHouseNumber()
		{
			var command = new AddResidenceCommand
			{
				HouseNumber = 22
			};

			Assert.Equal(22, command.HouseNumber);
		}

		[Fact]
		public void CommandShouldHaveAPlacename()
		{
			var command = new AddResidenceCommand
			{
				Placename = "Somecity"
			};

			Assert.Equal("Somecity", command.Placename);
		}

		[Fact]
		public void CommandShouldHaveAPostcode()
		{
			var command = new AddResidenceCommand
			{
				Postcode = 2222
			};

			Assert.Equal(2222, command.Postcode);
		}

		[Fact]
		public void CommandShouldHaveADescription()
		{
			var command = new AddResidenceCommand
			{
				Description = "Some description"
			};

			Assert.Equal("Some description", command.Description);
		}

		[Fact]
		public void CommandShouldHaveAResidenceType()
		{
			var command = new AddResidenceCommand
			{
				ResidenceType = "Appartment"
			};

			Assert.Equal("Appartment", command.ResidenceType);
		}
	}
}