using Xunit;
using RealEstate.Core.Application.Residencies.Commands;

namespace RealEstate.Test.Core.Application.Residencies.Commands
{
	public class AddResidenceCommandValidatorTest
	{
		[Fact]
		void LongitudeShouldBeRequired()
		{
			var command = new AddResidenceCommand
			{
				Longitude = 22.333,
				Latitude = 17.384,
				Street = "Somestreet",
				HouseNumber = 45,
				Placename = "Somecity",
				Postcode = 3434,
				Description = "Some description",
				ResidenceType = "Appartment"
			};
			var badcommand = new AddResidenceCommand
			{
				//Longitude = 22.333,
				Latitude = 17.384,
				Street = "Somestreet",
				HouseNumber = 45,
				Placename = "Somecity",
				Postcode = 3434,
				Description = "Some description",
				ResidenceType = "Appartment"
			};
			var cmdValidator = new AddResidenceCommandValidator(command);
			var badcmdValidator = new AddResidenceCommandValidator(badcommand);

			Assert.True(cmdValidator.IsValidCommand);
			Assert.False(badcmdValidator.IsValidCommand);
		}

		[Fact]
		void LatitudeShouldBeRequired()
		{
			var command = new AddResidenceCommand
			{
				Longitude = 22.333,
				Latitude = 17.384,
				Street = "Somestreet",
				HouseNumber = 45,
				Placename = "Somecity",
				Postcode = 3434,
				Description = "Some description",
				ResidenceType = "Appartment"
			};
			var badcommand = new AddResidenceCommand
			{
				Longitude = 22.333,
				//Latitude = 17.384,
				Street = "Somestreet",
				HouseNumber = 45,
				Placename = "Somecity",
				Postcode = 3434,
				Description = "Some description",
				ResidenceType = "Appartment"
			};
			var cmdValidator = new AddResidenceCommandValidator(command);
			var badcmdValidator = new AddResidenceCommandValidator(badcommand);

			Assert.True(cmdValidator.IsValidCommand);
			Assert.False(badcmdValidator.IsValidCommand);
		}

		[Fact]
		void StreetShouldBeRequired()
		{
			var command = new AddResidenceCommand
			{
				Longitude = 22.333,
				Latitude = 17.384,
				Street = "Somestreet",
				HouseNumber = 45,
				Placename = "Somecity",
				Postcode = 3434,
				Description = "Some description",
				ResidenceType = "Appartment"
			};
			var badcommand = new AddResidenceCommand
			{
				Longitude = 22.333,
				Latitude = 17.384,
				//Street = "Somestreet",
				HouseNumber = 45,
				Placename = "Somecity",
				Postcode = 3434,
				Description = "Some description",
				ResidenceType = "Appartment"
			};
			var cmdValidator = new AddResidenceCommandValidator(command);
			var badcmdValidator = new AddResidenceCommandValidator(badcommand);

			Assert.True(cmdValidator.IsValidCommand);
			Assert.False(badcmdValidator.IsValidCommand);
		}

		[Fact]
		void HouseNumberShouldBeRequired()
		{
			var command = new AddResidenceCommand
			{
				Longitude = 22.333,
				Latitude = 17.384,
				Street = "Somestreet",
				HouseNumber = 45,
				Placename = "Somecity",
				Postcode = 3434,
				Description = "Some description",
				ResidenceType = "Appartment"
			};
			var badcommand = new AddResidenceCommand
			{
				Longitude = 22.333,
				Latitude = 17.384,
				Street = "Somestreet",
				//HouseNumber = 45,
				Placename = "Somecity",
				Postcode = 3434,
				Description = "Some description",
				ResidenceType = "Appartment"
			};
			var cmdValidator = new AddResidenceCommandValidator(command);
			var badcmdValidator = new AddResidenceCommandValidator(badcommand);

			Assert.True(cmdValidator.IsValidCommand);
			Assert.False(badcmdValidator.IsValidCommand);
		}

		[Fact]
		void PlacenameShouldBeRequired()
		{
			var command = new AddResidenceCommand
			{
				Longitude = 22.333,
				Latitude = 17.384,
				Street = "Somestreet",
				HouseNumber = 45,
				Placename = "Somecity",
				Postcode = 3434,
				Description = "Some description",
				ResidenceType = "Appartment"
			};
			var badcommand = new AddResidenceCommand
			{
				Longitude = 22.333,
				Latitude = 17.384,
				Street = "Somestreet",
				HouseNumber = 45,
				//Placename = "Somecity",
				Postcode = 3434,
				Description = "Some description",
				ResidenceType = "Appartment"
			};
			var cmdValidator = new AddResidenceCommandValidator(command);
			var badcmdValidator = new AddResidenceCommandValidator(badcommand);

			Assert.True(cmdValidator.IsValidCommand);
			Assert.False(badcmdValidator.IsValidCommand);
		}

		[Fact]
		void PostcodeShouldBeRequired()
		{
			var command = new AddResidenceCommand
			{
				Longitude = 22.333,
				Latitude = 17.384,
				Street = "Somestreet",
				HouseNumber = 45,
				Placename = "Somecity",
				Postcode = 3434,
				Description = "Some description",
				ResidenceType = "Appartment"
			};
			var badcommand = new AddResidenceCommand
			{
				Longitude = 22.333,
				Latitude = 17.384,
				Street = "Somestreet",
				HouseNumber = 45,
				Placename = "Somecity",
				//Postcode = 3434,
				Description = "Some description",
				ResidenceType = "Appartment"
			};
			var cmdValidator = new AddResidenceCommandValidator(command);
			var badcmdValidator = new AddResidenceCommandValidator(badcommand);

			Assert.True(cmdValidator.IsValidCommand);
			Assert.False(badcmdValidator.IsValidCommand);
		}

		[Fact]
		void DescriptionShouldBeRequired()
		{
			var command = new AddResidenceCommand
			{
				Longitude = 22.333,
				Latitude = 17.384,
				Street = "Somestreet",
				HouseNumber = 45,
				Placename = "Somecity",
				Postcode = 3434,
				Description = "Some description",
				ResidenceType = "Appartment"
			};
			var badcommand = new AddResidenceCommand
			{
				Longitude = 22.333,
				Latitude = 17.384,
				Street = "Somestreet",
				HouseNumber = 45,
				Placename = "Somecity",
				Postcode = 3434,
				//Description = "Some description",
				ResidenceType = "Appartment"
			};
			var cmdValidator = new AddResidenceCommandValidator(command);
			var badcmdValidator = new AddResidenceCommandValidator(badcommand);

			Assert.True(cmdValidator.IsValidCommand);
			Assert.False(badcmdValidator.IsValidCommand);
		}

		[Fact]
		void ResidenceTypeShouldBeRequired()
		{
			var command = new AddResidenceCommand
			{
				Longitude = 22.333,
				Latitude = 17.384,
				Street = "Somestreet",
				HouseNumber = 45,
				Placename = "Somecity",
				Postcode = 3434,
				Description = "Some description",
				ResidenceType = "Appartment"
			};
			var badcommand = new AddResidenceCommand
			{
				Longitude = 22.333,
				Latitude = 17.384,
				Street = "Somestreet",
				HouseNumber = 45,
				Placename = "Somecity",
				Postcode = 3434,
				Description = "Some description"
				//ResidenceType = "Appartment"
			};
			var cmdValidator = new AddResidenceCommandValidator(command);
			var badcmdValidator = new AddResidenceCommandValidator(badcommand);

			Assert.True(cmdValidator.IsValidCommand);
			Assert.False(badcmdValidator.IsValidCommand);
		}
	}
}