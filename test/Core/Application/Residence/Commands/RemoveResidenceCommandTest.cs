using Xunit;
using RealEstate.Core.Application.Residencies.Commands;

namespace RealEstate.Test.Core.Application.Residencies.Commands
{
	public class RemoveResidenceCommandTest
	{
		[Fact]
		public void CommandShouldHaveAnId()
		{
			var command = new RemoveResidenceCommand
			{
				Id = 1
			};

			Assert.Equal(1, command.Id);
		}
	}
}