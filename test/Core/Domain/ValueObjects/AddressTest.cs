using Xunit;
using RealEstate.Core.Domain.ValueObjects;

namespace RealEstate.Test.Core.Domain.ValueObjects
{
    public class AddressTest
    {
        [Fact]
        public void AddressShouldHaveAStreet()
        {
            var address = new Address
            {
                Street = "Some Street"
            };
            Assert.Equal("Some Street", address.Street);
        }

        [Fact]
        public void AddressShouldHaveAHouseNumber()
        {
            var address = new Address
            {
                HouseNumber = 5
            };
            Assert.Equal(5, address.HouseNumber);
        }

        [Fact]
        public void AddressShouldHaveAPostCode()
        {
            var address = new Address
            {
                Postcode = 3234
            };
            Assert.Equal(3234, address.Postcode);
        }

        [Fact]
        public void AddressShouldHaveAPlacename()
        {
            var address = new Address
            {
                Placename = "Leuven"
            };
            Assert.Equal("Leuven", address.Placename);
        }
    }
}