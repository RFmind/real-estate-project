using Xunit;
using System;
using System.Collections.Generic;
using RealEstate.Core.Domain.Entities;
using RealEstate.Core.Domain.ValueObjects;

namespace RealEstate.Test.Core.Domain.Entities
{
    public class ResidenceTest
    {
        [Fact]
        public void ResidenceShouldHaveCoordinates()
        {
            var residence = new Residence
            {
                Coordinates = new List<double> { 12.222, 34.982 }
            };
            Assert.Equal(12.222, residence.Coordinates[0]);
        }

        [Fact]
        public void ResidenceShouldHaveAResidenceNumber()
        {
            var residence = new Residence
            {
                ResidenceNumber = 123
            };
            Assert.Equal(123, residence.ResidenceNumber);
        }

        [Fact]
        public void ResidenceShouldHaveAnAddress()
        {
            var exampleAddress = new Address
            {
                Street = "Some street",
                HouseNumber = 2,
                Postcode = 3000,
                Placename = "Leuven"
            };

            var residence = new Residence
            {
                Address = exampleAddress
            };
            Assert.Equal(exampleAddress, residence.Address);
        }

        [Fact]
        public void ResidenceShouldHaveADescription()
        {
            var residence = new Residence
            {
                Description = "Some description"
            };
            Assert.Equal("Some description", residence.Description);
        }

        [Fact]
        public void ResidenceShouldHaveAnId()
        {
            var residence = new Residence
            {
                Id = 1
            };

            Assert.Equal(1, residence.Id);
        }

        [Fact]
        public void ResidenceShouldHaveACreatedAt()
        {
            var now = DateTime.Now;
            var residence = new Residence
            {
                CreatedAt = now
            };

            Assert.Equal(now, residence.CreatedAt);
        }

        [Fact]
        public void ResidenceShouldHaveAVersion()
        {
            var version = 1;
            var residence = new Residence
            {
                Version = version
            };
            Assert.Equal(version, residence.Version);
        }

        [Fact]
        public void ResidenceShouldHaveResidenceType()
        {
            var exampleType = new AppartmentType{};
            var residence = new Residence
            {
                ResidenceType = exampleType
            };
            Assert.Equal(exampleType, residence.ResidenceType);
        }
    }
}